package com.demo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public class MarksController extends AbstractController {

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		ModelAndView modelAndView=new ModelAndView("output");
		modelAndView.addObject("sum",Integer.parseInt(request.getParameter("english"))+
				Integer.parseInt(request.getParameter("science"))+
				Integer.parseInt(request.getParameter("maths")));
		return modelAndView;
	}
}
