

public class Test {
	
	private String testid;
	private String testtitle;
	private int testmarks;
	public String getTestid() {
		return testid;
	}
	public void setTestid(String testid) {
		this.testid = testid;
	}
	public String getTesttitle() {
		return testtitle;
	}
	public void setTesttitle(String testtitle) {
		this.testtitle = testtitle;
	}
	public int getTestmarks() {
		return testmarks;
	}
	public void setTestmarks(int testmarks) {
		this.testmarks = testmarks;
	}
	
}
