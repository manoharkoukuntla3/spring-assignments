

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class display {

	public static void main(String[] args) {
	
		
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		
		Student s1 = (Student) context.getBean("student1");
		 s1.showinfo();
			
		 Student s2 = (Student) context.getBean("student2");
	
		 
		 s2.showinfo();
	}

}
