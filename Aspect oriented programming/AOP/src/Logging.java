
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;


@Aspect
public class Logging {
 
	@Pointcut("execution(public String getStudentid())")
	   private void selectAll(){}
	@Pointcut("execution(* *(..))")
	   private void selectOne(){}
	

   
   @After("selectAll()")
   public void afterAdvice(){
      System.out.println("Student profile has been setup.");
   }
  
   @Around("selectOne()")
   public Object profile(ProceedingJoinPoint pjp) throws Throwable {
           long start = System.currentTimeMillis();
           System.out.println("Method execution started at :" + start + " milliseconds." );
           Object output = pjp.proceed();
         
           long elapsedTime = System.currentTimeMillis() - start;
           System.out.println("Method execution time: " + elapsedTime + " milliseconds.");
           return output;
   }
   

   
   
}