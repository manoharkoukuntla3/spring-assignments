import java.util.List;

public class Student {

	private String studentid;
	private String studentname;
	private String studentadd;
	List<Student> stdList;

	public Student(){}
	
	public Student(String  studentid, String studentname, String studentadd) {
        this.studentid = studentid;
        this.studentname = studentname;
        this.studentadd = studentadd;
    }
	
	
	
 
    public String getStudentid() {
		return studentid;
	}

	public void setStudentid(String studentid) {
		this.studentid = studentid;
	}

	public String getStudentname() {
		return studentname;
	}

	public void setStudentname(String studentname) {
		this.studentname = studentname;
	}

	public String getStudentadd() {
		return studentadd;
	}

	public void setStudentadd(String studentadd) {
		this.studentadd = studentadd;
	}

	public List<Student> getStdList() {
		return stdList;
	}

	public void setStdList(List<Student> stdList) {
		this.stdList = stdList;
	}

	
	
	

    public void displayResult() {
        System.out.println("Student Details are :");
        
      
        	
        for (Student student : stdList) {
           System.out.println("Student ID :" + student.getStudentid() +"\n" + " Student Name: " + student.getStudentname()
           +"\n" + " Student ADD: " + student.getStudentadd());
        }
        
    }
	
	
	
	
	

}

