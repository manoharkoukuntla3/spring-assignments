

public class movieinfo {
	private String movieId;
	private String movieName;
	private String movieActor;
	
	public movieinfo(String movieId, String movieName, String movieActor) {
		super();
		this.movieId = movieId;
		this.movieName = movieName;
		this.movieActor = movieActor;
	}
	public movieinfo() {
		super();
	}
	
	public String getMovieId() {
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getMovieActor() {
		return movieActor;
	}
	public void setMovieActor(String movieActor) {
		this.movieActor = movieActor;
	}
	
	@Override
	public String toString() {
		return "movieId=" + movieId +"\n"+ "movieName=" + movieName +"\n"+ "movieActor=" + movieActor
				;
	}
	
	 
}
